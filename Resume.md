---
stylesheet: https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/2.10.0/github-markdown.min.css
body_class: markdown-body
css: |-
  .page-break { page-break-after: always; }
  .markdown-body { font-size: 11px; }
  .markdown-body pre > code { white-space: pre-wrap; }
  p {text-align:justify}
pdf_options:
  format: A4
  margin: 30mm 20mm
---


# Profile summary -  Vijay

<!-- A brief paragraph about oneself -->

* Demonstrated expertise in **researching, designing** and **launching** digital products/solutions in the Financial industry for over **12+ years**.
* Possess ability to bridge gap between design and technology by providing technical guidance during the design process and applying creative thinking and problem solving abilities to create opportunities for innovation.
* Proficient with requirements analysis, rapid visual and interaction prototyping, visual and non-visual component design, and successful implementation and integration of applications and services across presentation, logic, and data services tiers.
* Experience of working in multi-discipline teams including User Experience (UX) designers, Visual Designers, Interface Developers, Engineers, Data Architects, third-party technologists and other project managers to ensure solutions are delivered in alignment with the planned enterprise  framework.
* Excellent ability to communicate complex ideas and solutions to both business and technical audiences.

# Achievements

* Was awarded "Just do it" for two out of three ideas that were pitched for **BoS, 2019 Springboard** (MAS sponsored incubation for innovative and viable solutions)
* Directly involved in helping 10+ banks (Retail/Wealth space) go live with their digital transformation goals while primarily contributing towards performance, application usability, stability and over overall quality.
* Successfully completed **Temenos talent development program** - A Temenos initiative to recognise and groom the next generation of leaders.
* Developed IP and reusable assets including best practice guides for **Application performance, Rapid prototyping** and **Design driven delivery** to improve development process and generate high quality deliverables.

<!-- BOS, UOB, SCB, BMO, Rubik, MyState, Metro, Bukopin, EWB, CB -->


# Bank of Singapore, Singapore (June 2019 - Present)

## Solution design (UX), engineering & management @ Rapid Solution team

#### RESPONSIBILITY

* Collaborate with senior manaagement, business verticals and end users to identify business needs, user needs, study pain points and scope out a feasible and viable solution
* Introduce innovation by preparing and presenting proposals to stakeholders and navigating through approval processes
* Evaluate designs, new technologies, software products, proposals and conduct a buy vs build assessment 
* Manage to prioritize tasks and adjust directions to achieve both tactical and strategic goals in a changing environment
* Mentor and lead a team of Technology and Business associates to quickly turn around and buld MVPs with limited resources


#### Key Achievelemts in BoS in **three months**
* "Just do it" award was given for the **entrepreneurial** spirit demonstrated in identifying and solving key pain points/bottle necks of business users
** Contributed as a Solution designer and Visual designer for one of the pitches
** Contributed as a Technical architect and Visual deisgner for the other pitch



# Temenos, Singapore (2011-2019)

## Design and Channels technology specialist @ Digital Innovation Lab

#### RESPONSIBILITY
* Driving incubation of ideas in order to generate new client engagements in the digital space.
* Establish a research process, ensuring that it is consistently applied and continuously optimized and refined for improved outcomes.
* Explore emerging technology and keep abreast of industry trends to strategize and drive adoption of cutting edge technologies like **Augmented reality, Virtual reality, Internet of things, Conversational interfaces(Alexa) and Machine learning**.
* Working closely with key internal stakeholders to drive momentum and raise awareness by generating prototypes and proof of concepts.

> **Industry Knowledge:** Digital Strategy, Product Management, Agile Methodologies, Product innovation, Design Thinking practice

> **Tools & Technology:** Reactjs, Node, React Native, Apple ARKit, Google ARCore, Amazon Alexa, Mozilla A-frame, Adobe Creative Suite

<div class="page-break"></div>

## Digital transformation, Design consultant @ Digital Engagement Team

##### RESPONSIBILITY
* Engage, present concept and design deliverables to senior management at the VP-level and C-level.
* Lead workshops with end users & business with the goal of driving Digital transformation and identify Business/User needs.
* Generate high quality visual designs for prominent clients and/brands to help visualize and demonstrate business requirements.

> **Industry Knowledge:** Visual design, Rapid-prototyping, Interactive design principles, User interface patterns for digital applications,  Information architecture

> **Tools & Technology:** Temenos UXP, Temenos Connect suite (TCIB, TCMB), Design Studio, Integration framework, Interaction framework, Web socket, Cordova, Node, Android Studio, Swift, Git, Adobe Creative Suite

## Design, Digital SME and Technical consultant @ Expert services

##### RESPONSIBILITY
* Provide (design/technical) recommendations based on industry best practices, analytics, and user behaviour for internal teams and clients across APAC.
* Communicate, defend and build consensus around design direction and approach to all internal stakeholders.
* Partner with development teams to guide and support implementation of final designs/requirements.
* Critically analyse and review code/design deliverables from internal teams and external vendors and to provide relevant feedback in order to achieve desired outcomes.
* Act as hands-on design expert, shaping concepts & seeing them through from sketch to detailed  prototype.

> **Tools & Technology:** Application performance tuning, Responsive web design, Web services, Adobe XD, Illustrator, Axure RP, Temenos UXP, TCIB, TCMB, Node, Cordova, Mongo, Git


## Technical Lead @ Digital Channel Services

##### RESPONSIBILITY
* Lead, manage and inspire a team of developers to deliver on multiple concurrent projects from inception through completion.
* Partner with development teams to guide and implement/support implementation of final designs.
* Create mock-ups of elegant, functional and engaging user interfaces based on business requirements.
* Involve in creation of Functional Specification document.


> **Tools & Technology:** Temenos UXP, TCIB, TCMB, Responsive web design, Hybrid app development, API development, Bootstrap, jQuery, jQuery UI, ModernizrJS, Nodejs, Express, Cordova, Ajax, HTML5, CSS3, XSLT


<div class="page-break"></div>

# Dell Services/Perot Systems, India (2006-2011)

## Software development Advisor

##### RESPONSIBILITY
* Collaborate with product designers and UX specialists to refine specifications.
* Review, the estimation, time and cost of implementing specifications ensuring it meets the needs of the business.
* Translate specifications, visual designs/concepts into functional web applications.
* Design and implement services in SOA (Service-oriented architecture) using SOAP-based or RESTFULL based services.

> **Tools & Technology:** Java, j2ee, SOAP, XML, JUnit, HTML, CSS, XSLT, Javascript, jQuery, Linux, JBoss, Weblogic

## Software Engineer

##### RESPONSIBILITY
* Full-SDLC cycle, with large-scale live roll-out participation.
* Involve in system analysis, design, development and implementation of enterprise applications in Java and J2EE technologies.
* Develop & maintain the front-end of an existing web  application by taking requirements from the UX team and deliver required deliverables.
* Prepare Test plans and test scripts for unit tests and integration testing.

> **Tools & Technology:** Java, HTML, CSS, Javascript, Tomcat, JBoss, Databases

## Education

**Course in Machine Learning** - University of Edinburgh, UK (2009-2010)

**Bachelor of Engineering** - Bangalore Institute of Technology, Bangalore (2002-2006)

**Schooling** - Kendriya Vidyalaya NAL, Bangalore (1990-2002)


## Contact Information

**Email** - TBD

**Handphone** - TDB
